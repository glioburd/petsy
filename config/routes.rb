Rails.application.routes.draw do
  get 'pages/index'

  resources :posts do
    collection do
     get 'me'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'
  get '/animaux/:slug', to: 'posts#species', as: :species_posts
  get '/profil', to: 'users#edit', as: :profil
  patch '/profil', to: 'users#update'
  resources :users, only: [:new, :create] do
  	member do
  		get 'confirm'
  	end
  end
  # Sessions
  get '/login', to: 'sessions#new', as: :new_session
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy', as: :destroy_session
  resources :sessions, only: [:new, :create, :destroy]
  resources :passwords, only: [:new, :create, :edit, :update]
  resources :pets do
    resource :subscriptions, only: [:create, :destroy]
  end

  scope 'superadmin', module: 'admin', as: 'admin' do
    resources :species, except: [:show]
  end
end
