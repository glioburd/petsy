class ApplicationController < ActionController::Base
  # before_filter :set_cache_headers

  before_action :only_signed_in 
  protect_from_forgery with: :exception
  add_flash_types :success, :danger
  # Rendre accessible current_user au niveau des views
  helper_method :current_user, :user_signed_in?
  private
  	def only_signed_in
  		if !user_signed_in?
  			redirect_to new_user_path, danger: "Vous devez être connecté pour accéder à cette page."
  		end
  	end

  	def only_signed_out
  		redirect_to profil_path if user_signed_in?
  	end

  	def user_signed_in?
  		!current_user.nil?
  	end

  	# Retourne l'utilisateur actuellement connecté
  	def current_user
  		return nil if !session[:auth] || !session[:auth]["id"]
  		return @_user if @_user
  		# find_by renvoi nil au lieu d'une exception si l'id n'est pas trouvé.
  		# cela évite une erreur rails dans l'éventualité que l'id se trouve modifié quand un utilisateur est connecté.
  		@_user = User.find_by_id(session[:auth]["id"])
  	end
    
    # def set_cache_headers
    #   response.headers["Cache-Control"] = "no-cache, no-store"
    #   response.headers["Pragma"] = "no-cache"
    #   response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    # end
end
