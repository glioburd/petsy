class SubscriptionsController < ApplicationController

	before_action do
		@pet = Pet.find(params[:pet_id])
	end
	
	def create
		@pet.subscribers << current_user
		redirect_to @pet, success: "Subscribed"
	end

	def destroy
		@pet.subscribers.delete(current_user)
		redirect_to @pet, success: "Unsubscribed"
	end

end
