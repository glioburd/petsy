class User < ApplicationRecord

	has_many :pets, dependent: :destroy
	has_many :posts, dependent: :destroy
	has_many :subscriptions
	has_many :followed_pets, through: :subscriptions, source: :pet

	has_secure_password
	has_secure_token :confirmation_token
	has_secure_token :recover_password

	validates :username, 
	format: {with: /\A[a-zA-Z0-9_]{2,20}\z/, message: 'ne doit contenir que des caractères alphanumériques ou underscores.'},
	uniqueness: {case_sensitive: false}

	validates :email,
	format: {with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i},
	uniqueness: {case_sensitive: false}

	# Si avatar obligatoire, mettre presence: true
	
	has_image :avatar

	def to_session
		{id: id}
	end

end

